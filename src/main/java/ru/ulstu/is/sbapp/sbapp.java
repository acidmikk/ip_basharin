package ru.ulstu.is.sbapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class sbapp {

	public static void main(String[] args) {
		SpringApplication.run(sbapp.class, args);
	}

	@GetMapping("/pow")
    public String hello(@RequestParam(value = "num", defaultValue = "5") Integer num) {
        return String.valueOf(num * num);
	}
}